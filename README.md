# Quarkus app with brotli support for native images

## Running the application

Using docker compose:

* Windows:

```shell
docker-compose run build-windows
docker-compose up windows
```

* Linux:

```shell
docker-compose run build-linux
docker-compose up linux
```

* Linux statically linked:

```shell
docker-compose run build-static
docker-compose up static
```

## Sample payloads

* Windows (powershell):

```powershell
Invoke-RestMethod 'http://localhost:8080/brotli' -Method 'POST' -Body "Hello world!"
```

* Linux:

```shell
curl --request POST 'http://localhost:8080/brotli' --data-raw 'Hello World!'
```
