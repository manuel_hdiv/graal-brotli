package org.hdiv.quarkus;

import io.quarkus.test.junit.NativeImageTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@NativeImageTest
public class NativeResourcesTest {

	@Test
	public void testHelloEndpoint() {
		given().when().body("hello").post("/echo").then().statusCode(200).body(is("hello"));
	}

	@Test
	public void testBrotliEndpoint() {
		given().when().body("hello").post("/brotli").then().statusCode(200).body(is("hello"));
	}
}
