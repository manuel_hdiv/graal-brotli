package org.hdiv.quarkus;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/echo")
public class EchoResource {

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String execute(final String payload) {
		System.out.println("Calling echo");
		return payload;
	}
}
