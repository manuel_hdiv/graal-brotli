package org.hdiv.quarkus;

import java.io.IOException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aayushatharva.brotli4j.Brotli4jLoader;
import com.aayushatharva.brotli4j.decoder.Decoder;
import com.aayushatharva.brotli4j.encoder.Encoder;

@Path("/brotli")
public class BrotliResource {

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String execute(final String payload) throws IOException {
		System.out.println("Calling brotli");
		Brotli4jLoader.ensureAvailability();
		byte[] compressed = Encoder.compress(payload.getBytes());
		return new String(Decoder.decompress(compressed).getDecompressedData());
	}
}
